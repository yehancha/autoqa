<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of auth
 *
 * @author Yehan
 */
class Auth extends CI_Controller {

  /**
   * Constructor: loads URL helper and user model
   */
  function __construct() {
    parent::__construct();
    $this->load->helper('url');    
    $this->load->model('user', '', TRUE);
  }

  /**
   * Show the register_view page
   */
  public function new_user() {
    $this->load->view('register_view');
  }

  /**
   * Registers a new user using given details
   */
  public function register() {
    $name = $this->input->post('name', TRUE);
    $username = $this->input->post('username', TRUE);
    $password = $this->input->post('password', TRUE);

    $result = $this->user->register($name, $username, $password);
    if ($result->error == null) {
      redirect('/auth/login');
    } else {
      $data['error'] = $result->error;
      $this->load->view('register_view', $data);
    }
  }
  
  /**
   * Logs in the user after authenticating given information.
   */
  public function login() {
    $username = $this->input->post('username', TRUE);
    $password = $this->input->post('password', TRUE);
    
    if (!$username || !$password) {
      $this->load->view('login_view');
      return;
    }
    
    $result = $this->user->login($username, $password);
    if ($result->error == NULL) {
      $redirect_url = $this->user->get_redirect_url();
      if ($redirect_url === NULL || $redirect_url == '') {
        redirect('/');
      } else {
        redirect($redirect_url);
      }
    } else {
      $data['error'] = $result->error;
      $this->load->view('login_view', $data);
    }
  }
  
  /**
   * Logs out the current user.
   */
  public function logout() {
    $this->user->logout();
    $this->load->helper('url');
    redirect('/');
  }
  
  /**
   * Checks whether a user is logged in using this session.
   */
  public function is_logged_in() {
    return $this->user->is_logged_in();
  }

}

?>
