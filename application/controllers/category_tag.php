<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of category_tags
 *
 * @author Yehan
 */
class Category_tag extends CI_Controller {
  /**
   * Shows the category_tag page
   */
  public function index() {
    $this->load->model('user', '', TRUE);
    $data['user'] = $this->user->get_current_user();
    
    $this->load->model('category', '', TRUE);
    $data['categories'] = $this->category->get_all();
    
    $this->load->model('tag', '', TRUE);
    $data['tags'] = $this->tag->get_all();
    
    $this->load->view('category_tag', $data);
  }
}

?>
