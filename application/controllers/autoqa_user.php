<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of user
 *
 * @author Yehan
 */
class Autoqa_user extends CI_Controller {
  /**
   * Shows the user profile of a given user.
   */
  function view() {
    $id = $this->input->get('id', TRUE);
    
    $this->load->model('user', '', TRUE);
    $data['user'] = $this->user->get_current_user();
    
    if (!isset($id) || $id == NULL || $id == '') {
      $data['error'] = '<h1>Oops!</h1><p>Cannot find the User you requested.</p><p>Please click <a href="javascript:history.back()">here</a> to go back to where you were OR press the back button of your browser</p>';
      $this->load->view('error_page', $data);
      return;
    }
    
    $this->load->model('user', '', TRUE);
    $user_data = $this->user->get($id);
    
    if (!isset($user_data) || $user_data == NULL || isset($user_data->error)) {
      $data['error'] = '<h1>Oops!</h1><p>Cannot find the User you requested.</p><p>Please click <a href="javascript:history.back()">here</a> to go back to where you were OR press the back button of your browser</p>';
      $this->load->view('error_page', $data);
      return;
    }
    
    $data['user_data'] = $user_data;
    
    $this->load->view('user_view', $data);
  }
}

?>
