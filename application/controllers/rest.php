<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of rest
 *
 * @author Yehan
 */
class Rest extends CI_Controller {

   /**
    * Re-map the URL to proper functions.
    * URL Structure
    * =============
    * rest/questions/[question_id] - redirect to question page
    * rest/questions/[question_id]/votes - get votes, vote, clear vote
    * rest/answers/[answer_id]/ratings - get or clear rating
    * rest/answers/[answer_id]/ratings/[rating] - rate
    */
  function _remap() {
    $segment_count = $this->uri->total_segments();
    if ($segment_count == 1) {
      $this->index();
    }

    $this->load->helper('url');
    $main_segment = $this->uri->segment(2);
    $bad_url = false;

    if ($main_segment == 'answers' && $segment_count > 3 && $this->uri->segment(4) == 'ratings') {
      // Work with answers
      $answer_id = $this->uri->segment(3);
      if ($this->input->server('REQUEST_METHOD') == 'GET') {
        return $this->get_ratings($answer_id);
      } else if ($this->input->server('REQUEST_METHOD') == 'PUT' && $segment_count == 5) {
        return $this->rate($answer_id, $this->uri->segment(5));
      } else if ($this->input->server('REQUEST_METHOD') == 'DELETE') {
        return $this->clear_rating($answer_id);
      } else {
        $bad_url = true;
      }
    } else if ($main_segment == 'questions') {
      // Work eith questions
      $question_id = $this->uri->segment(3);
      if ($segment_count == 2) {
        redirect(site_url('/question'));
      } else if ($segment_count == 3) {
        redirect(site_url("/question/view?id=$question_id"));
      } else if ($segment_count == 4 && $this->uri->segment(4) == 'votes') {
        if ($this->input->server('REQUEST_METHOD') == 'GET') {
          return $this->get_votes($question_id);
        } else if ($this->input->server('REQUEST_METHOD') == 'PUT') {
          return $this->vote($question_id);
        } else if ($this->input->server('REQUEST_METHOD') == 'DELETE') {
          $this->clear_vote($question_id);
        }
      } else {
        $bad_url = true;
      }
    } else {
      $bad_url = true;
    }

    if ($bad_url) {
      show_404();
    }
  }

  function index() {
    // show rest help page
  }

  /**
   * Get the number of votes for the given question.
   * @param type $question_id Id of the question which the votes are needed for
   */
  function get_votes($question_id) {
    $this->load->model('question_model', '', TRUE);
    echo json_encode($this->question_model->get_votes($question_id));
  }

  /**
   * Add a vote for a given question.
   * @param type $question_id Id of the question to be voted.
   */
  function vote($question_id) {
    $this->load->model('question_model', '', TRUE);
    $message = $this->question_model->vote($question_id);
    echo $message;
  }

  /**
   * Removes a vote from the given question.
   * @param type $question_id Id of the question which the vote needs to be deleted.
   */
  function clear_vote($question_id) {
    $this->load->model('question_model', '', TRUE);
    $message = $this->question_model->clear_vote($question_id);
    echo $message;
  }

  /**
   * Get rating for the given answer
   * @param type $answer_id Id of the answer which the rating are needed.
   */
  function get_ratings($answer_id) {
    $this->load->model('answer_model', '', TRUE);
    echo json_encode($this->answer_model->get_ratings($answer_id));
  }

  /**
   * Rate the given answer.
   * @param type $answer_id Id of the answer to be rated
   * @param type $rating Actual rating
   */
  function rate($answer_id, $rating) {
    $this->load->model('answer_model', '', TRUE);
    echo json_encode($this->answer_model->rate($answer_id, $rating));
  }

  /**
   * Removes a rating from the given answer
   * @param type $answer_id Id of the answer which the rating needs to be removed.
   */
  function clear_rating($answer_id) {
    $this->load->model('answer_model', '', TRUE);
    echo json_encode($this->answer_model->clear_rating($answer_id));
  }

}

?>
