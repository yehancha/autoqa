<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of answer
 *
 * @author Yehan
 */
class Answer extends CI_Controller{
  
  /**
   * Adds a given answer to a given question. If there is no user logged in,
   * user will be redirected to the login page.
   */
  public function add() {
    $this->load->model('user', '', TRUE);
    $user = $this->user->get_current_user();
    if ($user == NULL) {
      // If the user is not logged in we will send him to the login page and he
      // will redirected back to the question page after login in.
      $this->redirect_to_login();
    }
    
    $answer_id = NULL;
    if ($this->input->post()) {
      // We use TRUE as the 2nd parameter to get the data out through XSS filter
      $answer = $this->input->post('answer', TRUE);
      $question_id = $this->input->post('question_id', TRUE);
      $this->load->model('answer_model', '', TRUE);
      $answer_id = $this->answer_model->add($answer, $question_id, $user);
    }
    
    redirect(site_url("question/view?id=$question_id#answer$answer_id"));
  }
  
  /**
   * Redirects the user to the login page. Current URL will be stored and the
   * user will be redirected to that URL after a successful login.
   */
  function redirect_to_login() {
    $this->load->helper('url');
    $id = $this->input->get('question_id', TRUE);
    $this->user->set_redirect_url(site_url("question/view?id=$id"));
    redirect('/auth/login');
  }
  
  /**
   * Shows the edit_answer page. User will be redirected to the login page
   * if currently not logged in.
   */
  function edit() {
    $id = $this->input->get('id', TRUE);
    if ($id == NULL || $id == '') {
      $data['error'] = '<h1>Oops!</h1><p>Cannot find the Answer you are trying to edit.</p><p>Please click <a href="javascript:history.back()">here</a> to go back to where you were OR press the back button of your browser</p>';
      $this->load->view('error_page', $data);
      return;
    }
    
    $this->load->model('user', '', TRUE);
    $user = $this->user->get_current_user();
    
    if (!isset($user) || $user == NULL) {
      $this->load->helper(url);
      $this->user->set_redirect_url(site_url("/answer/edit?id=$id"));
      redirect('/auth/login');
    }
    
    $data['user'] = $user;
    
    $this->load->model('answer_model', '', TRUE);
    $answer = $this->answer_model->get($id);
    
    if (!isset($answer) || $answer == NULL) {
      $data['error'] = '<h1>Oops!</h1><p>Cannot find the Answer you are trying to edit.</p><p>Please click <a href="javascript:history.back()">here</a> to go back to where you were OR press the back button of your browser</p>';
      $this->load->view('error_page', $data);
      return;
    } else if ($answer->user_id != $user->id) {
      $data['error'] = '<h1>Oops!</h1><p>You are not allowed to edit this answer.</p><p>Please click <a href="javascript:history.back()">here</a> to go back to where you were OR press the back button of your browser</p>';
      $this->load->view('error_page', $data);
      return;
    }
    
    $data['answer'] = $answer;
    
    $this->load->view('edit_answer', $data);
  }
  
  /**
   * Updates a given answer after authenticating and authorizing the user.
   */
  function update() {
    $id = NULL;
    $answer = NULL;
    
    if ($this->input->post()) {
      $id = $this->input->post('id', TRUE);
      $answer = $this->input->post('answer', TRUE);
    }
    
    if (!isset($id) || $id == NULL || !isset($answer) || $answer == NULL) {
      $data['error'] = '<h1>Oops!</h1><p>Insufficient data to save the Answer.</p><p>Please click <a href="javascript:history.back()">here</a> to go back to where you were OR press the back button of your browser</p>';
      $this->load->view('error_page', $data);  
    }
    
    $this->load->model('user', '', TRUE);
    $user = $this->user->get_current_user();
    
    if (!isset($user) || $user == NULL) {
      $this->load->helper(url);
      $this->user->set_redirect_url(site_url("/answer/edit?id=$id"));
      redirect('/auth/login');
    }
    
    $data['user'] = $user;
    
    $this->load->model('answer_model', '', TRUE);
    $result = $this->answer_model->update($id, $answer, $question_id, $user);
    
    if (isset($result->error) && $result->error != NULL) {
      $data['error'] = $result->error;
      $this->load->view('error_page', $data);
      return;      
    }
    
    $this->load->helper('url');
    redirect("/question/view?id=$result->question_id#answer$id");
  }
  
  /**
   * Deleting an answer after authenticating and authorizing the user.
   */
  function delete() {
    $this->load->model('user', '', TRUE);
    $user = $this->user->get_current_user();
    
    if (!isset($user) || $user == NULL) {
      $this->user->set_redirect_url(current_url());
      redirect('/auth/login');
    }
    
    $data['user'] = $user;
    
    $id = $this->input->get('id');
    if (!isset($id) || $id == NULL) {
      $data['error'] = '<h1>Oops!</h1><p>Cannot find the Answer you are trying to delete.</p><p>Please click <a href="javascript:history.back()">here</a> to go back to where you were OR press the back button of your browser</p>';
      $this->load->view('error_page', $data);
      return;
    }
    
    $this->load->model('answer_model', '', TRUE);
    $result = $this->answer_model->delete($id);
    
    if (isset($result->error) || $result->error != NULL) {
      $data['error'] = "<h1>Oops!</h1><p>$result->error</p><p>Please click <a href='javascript:history.back()'>here</a> to go back to where you were OR press the back button of your browser</p>";
      $this->load->view('error_page', $data);
      return;
    } 
    
    redirect("/question/view?id=$result->question_id");
  }
}

?>
