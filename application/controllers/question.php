<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of question
 *
 * @author Yehan
 */
class Question extends CI_Controller {

  /**
   * Constructor: loads user model
   */
  public function __construct() {
    parent::__construct();
    $this->load->model('user', '', TRUE);
  }

  /**
   * Shows the home page
   */
  public function index() {
    // loading initial data
    $this->load->model('question_model', '', TRUE);
    $questions = $this->question_model->search('');
    $data['questions'] = $questions;
    $data['user'] = $this->user->get_current_user();
    $this->load->view('question_list', $data);
  }

  /**
   * Shows the ask_question_view page
   */
  public function ask() {
    $this->load->helper('url');

    $this->load->model('user');
    if (!$this->user->is_logged_in()) {
      $this->user->set_redirect_url(current_url());
      redirect('/auth/login');
    }

    if ($this->input->post()) { // If the form data received
      // Process data
      // We use TRUE as the 2nd parameter to get the data out through XSS filter
      $question = $this->input->post('question', TRUE);
      $description = $this->input->post('description', TRUE);
      $category_id = $this->input->post('category', TRUE);
      $tags = $this->input->post('tag', TRUE);

      $this->load->model('Question_model', '', TRUE);
      $question_id = $this->Question_model->add($question, $description, $category_id, $tags);

      redirect("/question/view?id=$question_id");
    } else { // If the form data not received
      // We show the form
      $this->load->model('Category', '', TRUE);
      $data['categories'] = $this->Category->get_all();
      $data['user'] = $this->user->get_current_user();

      $this->load->view('ask_question_view', $data);
    }
  }

  /**
   * Shows the question_view page with the information of the given question
   */
  public function view() {
    $id = $this->input->get('id', TRUE);
    $this->load->model('question_model', '', TRUE);
    $question = $this->question_model->get($id);

    if ($question) {
      $data['question'] = $question;
      $data['is_logged_in'] = $this->user->is_logged_in();
      $data['user'] = $this->user->get_current_user();
      $this->load->view('question_view', $data);
    } else {
      $data['error'] = '<h1>Oops!</h1><p>Cannot find the Question you are looking for.</p><p>Please click <a href="javascript:history.back()">here</a> to go back to where you were OR press the back button of your browser</p>';
      $this->load->view('error_page', $data);
      return;
    }
  }

  /**
   * Searches questions for a given search term and shows the question list
   * page with results.
   */
  public function search() {
    $q = $this->input->get('q', TRUE);
    $sort = $this->input->get('sort', TRUE);
    $category_id = $this->input->get('category_id', TRUE);
    $this->load->model('question_model', '', TRUE);
    $questions = $this->question_model->search($q, $sort, $category_id);
    $data['questions'] = $questions;
    $data['q'] = $q;
    $data['sort'] = $sort;
    $data['category_id'] = $category_id;
    $data['user'] = $this->user->get_current_user();
    $this->load->view('question_list', $data);
  }
  
  /**
   * Shows the edit_question_view page with the information of the given
   * question after autheticating the user.
   */
  function edit() {
    $id = $this->input->get('id', TRUE);
    if ($id == NULL || $id == '') {
      $data['error'] = '<h1>Oops!</h1><p>Cannot find the Question you are trying to edit.</p><p>Please click <a href="javascript:history.back()">here</a> to go back to where you were OR press the back button of your browser</p>';
      $this->load->view('error_page', $data);
      return;
    }

    $user = $this->user->get_current_user();

    if (!isset($user) || $user == NULL) {
      $this->load->helper(url);
      $this->user->set_redirect_url(site_url("/question/edit?id=$id"));
      redirect('/auth/login');
    }

    $data['user'] = $user;

    $this->load->model('question_model', '', TRUE);
    $question = $this->question_model->get($id, FALSE, FALSE); // We don't need to replace new lines characters, we don't need answers

    if (!isset($question) || $question == NULL) {
      $data['error'] = '<h1>Oops!</h1><p>Cannot find the Question you are trying to edit.</p><p>Please click <a href="javascript:history.back()">here</a> to go back to where you were OR press the back button of your browser</p>';
      $this->load->view('error_page', $data);
      return;
    } else if ($question->user_id != $user->id) {
      $data['error'] = '<h1>Oops!</h1><p>You are not allowed to edit this question.</p><p>Please click <a href="javascript:history.back()">here</a> to go back to where you were OR press the back button of your browser</p>';
      $this->load->view('error_page', $data);
      return;
    }

    $this->load->model('category', '', TRUE);
    $categories = $this->category->get_all();
    $data['categories'] = $categories;

    $this->load->model('tag', '', TRUE);
    $tags = $this->tag->get_tags_to_question($id);
    $question->tags = $tags;

    $data['question'] = $question;

    $this->load->view('ask_question_view', $data);
  }

  /**
   * Updates the given quesition with new data and forwards back the user to the
   * question page.
   */
  function update() {
    $id = NULL;
    $question = NULL;
    $description = NULL;
    $category_id = NULL;
    $tags = NULL;
    
    if ($this->input->post()) {
      $id = $this->input->post('id', TRUE);
      $question = $this->input->post('question', TRUE);
      $description = $this->input->post('description', TRUE);
      $category_id = $this->input->post('category', TRUE);
      $tags = $this->input->post('tag', TRUE);
    }
    
    if (!isset($id) || $id == NULL
            || !isset($question) || $question == NULL
            || !isset($description) || $description == NULL
            || !isset($category_id) || $category_id == NULL) {
      $data['error'] = '<h1>Oops!</h1><p>Insufficient data to save the question.</p><p>Please click <a href="javascript:history.back()">here</a> to go back to where you were OR press the back button of your browser</p>';
      $this->load->view('error_page', $data);  
    }

    $this->load->model('user', '', TRUE);
    $user = $this->user->get_current_user();

    if (!isset($user) || $user == NULL) {
      $this->load->helper(url);
      $this->user->set_redirect_url(site_url("/question/edit?id=$id"));
      redirect('/auth/login');
    }
    
    $data['user'] = $user;
    
    $this->load->model('question_model', '', TRUE);
    $result = $this->question_model->update($id, $question, $description, $category_id, $tags, $user);

    if (isset($result->error) && $result->error != NULL) {
      $data['error'] = $result->error;
      $this->load->view('error_page', $data);
      return;      
    }
    
    $this->load->helper('url');
    redirect("/question/view?id=$id");
  }
  
  /**
   * Deltes the given question after authenticating the user.
   */
  function delete() {
    $this->load->model('user', '', TRUE);
    $user = $this->user->get_current_user();
    
    if (!isset($user) || $user == NULL) {
      $this->user->set_redirect_url(current_url());
      redirect('/auth/login');
    }
    
    $data['user'] = $user;
    
    $id = $this->input->get('id');
    if (!isset($id) || $id == NULL) {
      $data['error'] = '<h1>Oops!</h1><p>Cannot find the Question you are trying to delete.</p><p>Please click <a href="javascript:history.back()">here</a> to go back to where you were OR press the back button of your browser</p>';
      $this->load->view('error_page', $data);
      return;
    }
    
    $this->load->model('question_model', '', TRUE);
    $error = $this->question_model->delete($id);
    
    if (isset($error) || $error != NULL) {
      $data['error'] = "<h1>Oops!</h1><p>$error</p><p>Please click <a href='javascript:history.back()'>here</a> to go back to where you were OR press the back button of your browser</p>";
      $this->load->view('error_page', $data);
    } 
    
    redirect('/');
  }

}

?>
