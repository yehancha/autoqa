<html>
  <head>
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-theme.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/autoqa.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/question_view.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/star-rating.min.css'); ?>" />
    <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/star-rating.min.js') ?>"></script>
  </head>
  <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/autoqa/">AutoQA</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="/autoqa/">Home</a></li>
            <li><a href="/autoqa/index.php/question/ask">Ask</a></li>
            <li><a href="/autoqa/index.php/category_tag">Categories/Tags</a></li>
          </ul>
          <form class="navbar-form navbar-left" role="search" action="/autoqa/index.php/question/search" method="GET">
            <div class="form-group">
              <input name="q" id="q" type="text" class="form-control" placeholder="Search" size="50">
            </div>
          </form>
          <ul class="nav navbar-nav navbar-right <?php if (isset($user) && $user != NULL) echo 'hidden' ?>">
            <li><a href="/autoqa/index.php/auth/login">Login</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right <?php if (!isset($user) || $user == NULL) echo 'hidden' ?>">
            <li><a href="/autoqa/index.php/auth/logout">Logout</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right <?php if (!isset($user) || $user == NULL) echo 'hidden' ?>">
            <li><a href="/autoqa/index.php/autoqa_user/view?id=<?php if (isset($user) && $user != NULL) echo $user->id ?>"><?php if (isset($user) && $user != NULL) echo $user->name ?></a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container">
      <div class="question-main">
        <div class="row">
          <div class="col-sm-2 question-sidebar question-sidebar-top-left">
            <p><span class="label <?php
              if (isset($question->has_voted) && $question->has_voted == TRUE) {
                echo 'label-warning';
              } else {
                echo 'label-default';
              }
              ?>" id="votes"></span></p>
                     <?php
                     foreach ($question->tags as $tag) {
                       echo "<a class='tag' href='/autoqa/index.php/question/search?q=tag%3D$tag'><span class='label label-default tag'>$tag</span></a>\n";
                     }
                     ?>
          </div>
          <div class="col-sm-8 question-post">
            <h2 class="question-post-title"><?php echo $question->question ?></h2>
            <p class="question-post-meta category">
              <?php echo "<a href='/autoqa/index.php/question/search?category_id=$question->category_id'>in $question->category_name</a>"; ?>
            </p>
            <p>
              <?php echo $question->description ?><br />
            </p>
          </div>
          <div class="col-sm-2 question-sidebar question-sidebar-top-right">
            <p class="question-post-meta">
              <?php echo "<time>$question->time</time>"; ?>
            </p>
            <p class="question-post-meta no-dec">
              <a href='/autoqa/index.php/autoqa_user/view?id=<?php echo $question->user_id; ?>'><?php echo $question->user_name; ?></a>
            </p>
            <p class='question-post-meta no-dec'>
              <?php
              if (isset($user) && $user != NULL && $user->id == $question->user_id) {
                echo "| <a href='/autoqa/index.php/question/edit?id=$question->id'>edit</a> ";
              }
              if (isset($user) && $user != NULL && ($user->id == $question->user_id || $user->type == 'admin')) {
                echo "| <a href='/autoqa/index.php/question/delete?id=$question->id' class='delete'>delete</a> |";
              }
              ?>
            </p>
          </div>
        </div>

        <hr />

        <?php
        if (isset($question->answers) && count($question->answers) > 0)
          echo '<div class = "row">' . "\n"
          . '<div class = "col-sm-8 col-sm-offset-2 question-post">' . "\n"
          . '<h3>Answer(s)</h3>' . "\n"
          . '</div></div>' . "\n";
        ?>

        <?php
        foreach ($question->answers as $answer) {
          echo '<div class="row">' . "\n"
          . '<div class="col-sm-2 question-sidebar">' . "\n"
          . "<div class='text-center'><div class='rating-container'><input id='rating$answer->id' type='number' class='rating' min='0' max='5' step='1' data-size='xs' data-symbol='&#9733;' data-show-caption='false'></div></div>"
          . "<div id='userRating$answer->id' class='text-center'></div>\n"
          . '</div>' . "\n"
          . '<div class="col-sm-8 question-post">' . "\n"
          . "<p id='answer$answer->id'>$answer->answer</p>\n"
          . '</div>' . "\n"
          . '<div class="col-sm-2 question-sidebar">' . "\n"
          . "<p class='question-post-meta'><time>$answer->time</time></p>\n"
          . "<p class='question-post-meta no-dec'><a href='/autoqa/index.php/autoqa_user/view?id=$answer->user_id'>$answer->user_name</a></p>\n"
          . "<p class='question-post-meta no-dec'>";

          if (isset($user) && $user != NULL && $user->id == $answer->user_id) {
            echo "| <a href='/autoqa/index.php/answer/edit?id=$answer->id'>edit</a> ";
          }
          if (isset($user) && $user != NULL && ($user->id == $answer->user_id || $user->type == 'admin')) {
            echo "| <a href='/autoqa/index.php/answer/delete?id=$answer->id' class='delete'>delete</a> |";
          }

          echo '</p></div>' . "\n"
          . '</div><hr />' . "\n";
        }
        ?>

        <?php
        if (isset($user) && $user != NULL) {
          echo '<div class="row"><div class="col-sm-8 col-sm-offset-2">' . "\n"
          . '<form class="form-ask" action="/autoqa/index.php/answer/add" method="POST">' . "\n"
          . '<div class="form-group">' . "\n"
          . '<label for="textareaAnswer" class="sr-only">Answer</label>' . "\n"
          . '<textarea name="answer" id="textareaAnswer" class="form-control" rows="10" placeholder="You have an answer? Post it here." required></textarea><br />' . "\n"
          . '</div>' . "\n"
          . '<button class="btn btn-lg btn-primary btn-block" type="submit">Answer</button>' . "\n"
          . "<input name='question_id' hidden value='$question->id'>" . "\n"
          . '</form>'
          . '</div></div>';
        } else {
          echo '<div class="row"><div class="col-sm-8 col-sm-offset-2 jumbotron">'
          . "<h3>I have an <a href='/autoqa/index.php/answer/add?question_id=$question->id' role='button'>Answer!</a></h3>"
          . '</div></div>';
        }
        ?>
      </div>
    </div>
    <script type="text/javascript">
      $(document).ready(function() {
        $('time').each(function() {
          // Unix timestamp is in seconds. But Date object expect miliseconds.
          // So have to multiply by 1000
          var date = new Date(parseInt($(this).html()) * 1000);
          $(this).html(date.toLocaleTimeString() + '<br />' + date.toLocaleDateString());
        });

        $.ajax({
          url: '/autoqa/index.php/rest/questions/<?php echo $question->id ?>/votes',
          success: function(data) {
            var result = $.parseJSON(data);
            $('#votes').html(result.votes);
          },
          async: true,
          type: 'GET'
        });

        $('[id^=rating]').each(function() {
          //        alert($(this).attr('id'));
          var answerId = $(this).attr('id');
          var answerId = answerId.replace('rating', '');
          $.ajax({
            url: '/autoqa/index.php/rest/answers/' + answerId + '/ratings',
            success: function(data) {
              setRating(jQuery.parseJSON(data));
            },
            async: true,
            type: 'GET'
          });
        });
      });

      $('.delete').click(function() {
        return confirm("You cannot undo deletions. Are you sure you want to delete?");
      });

      $('#votes').click(function() {
        if ($(this).hasClass('label-default')) {
          vote(this);
        } else {
          clearVote(this);
        }
      });

      $('[id^=rating]').on('rating.change', function(event, value, caption) {
        var answerId = $(this).attr('id');
        var answerId = answerId.replace('rating', '');
        $.ajax({
          url: '/autoqa/index.php/rest/answers/' + answerId + '/ratings/' + value,
          success: function(data) {
            var rating = jQuery.parseJSON(data);
            if (rating.error != null) {
              alert(rating.error);
            }
            setRating(rating);
          },
          error: function(jqXHR, textStatus, errorThrown) {
            alert(errorThrown);
          },
          async: true,
          type: 'PUT'
        });
      });

      $('[id^=rating]').on('rating.clear', function(event) {
        var answerId = $(this).attr('id');
        var answerId = answerId.replace('rating', '');
        $.ajax({
          url: '/autoqa/index.php/rest/answers/' + answerId + '/ratings',
          success: function(data) {
            var rating = jQuery.parseJSON(data);
            if (rating.error != null) {
              alert(rating.error);
            }
            setRating(rating);
          },
          error: function(jqXHR, textStatus, errorThrown) {
            alert(errorThrown);
          },
          async: true,
          type: 'DELETE'
        });
      });

      function vote(label) {
        $.ajax({
          url: '/autoqa/index.php/rest/questions/<?php echo $question->id ?>/votes',
          success: function(data) {
            if (data == '1') {
              $(label).removeClass('label-default');
              $(label).addClass('label-warning');
              $(label).html(parseInt($(label).html()) + 1);
            } else {
              alert(data);
            }
          },
          error: function(jqXHR, textStatus, errorThrown) {
            alert(errorThrown);
          },
          async: true,
          type: 'PUT'
        });
      }

      function clearVote(label) {
        $.ajax({
          url: '/autoqa/index.php/rest/questions/<?php echo $question->id ?>/votes',
          success: function(data) {
            if (data == '1') {
              $(label).removeClass('label-warning');
              $(label).addClass('label-default');
              $(label).html(parseInt($(label).html()) - 1);
            } else {
              alert(data);
            }
          },
          error: function(jqXHR, textStatus, errorThrown) {
            alert(errorThrown);
          },
          async: true,
          type: 'DELETE'
        });
      }

      function setRating(rating) {
        $('#rating' + rating.answer_id).rating('update', rating.rating);
        if (rating.user_rating != null) {
          $('#userRating' + rating.answer_id).html('<span class="label label-info">You rateda at ' + rating.user_rating + '</span>');
        } else {
          $('#userRating' + rating.answer_id).html('');
        }
      }
    </script>
  </body>
</html>