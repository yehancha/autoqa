<html>
  <head>
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-theme.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/autoqa.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/question_list.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/star-rating.min.css'); ?>" />
    <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/star-rating.min.js') ?>"></script>
  </head>
  <body>
    <?php
    // variable initialising
    if (!isset($q))
      $q = NULL;
    if (!isset($sort))
      $sort = NULL;
    ?>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/autoqa/">AutoQA</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="/autoqa/">Home</a></li>
            <li><a href="/autoqa/index.php/question/ask">Ask</a></li>
            <li><a href="/autoqa/index.php/category_tag">Categories/Tags</a></li>
          </ul>
          <form class="navbar-form navbar-left" role="search" action="/autoqa/index.php/question/search" method="GET">
            <div class="form-group">
              <input name="q" id="q" type="text" class="form-control" placeholder="Search" size="50" value="<?php echo $q; ?>">
            </div>
          </form>
          <ul class="nav navbar-nav navbar-right <?php if (isset($user) && $user != NULL) echo 'hidden' ?>">
            <li><a href="/autoqa/index.php/auth/login">Login</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right <?php if (!isset($user) || $user == NULL) echo 'hidden' ?>">
            <li><a href="/autoqa/index.php/auth/logout">Logout</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right <?php if (!isset($user) || $user == NULL) echo 'hidden' ?>">
            <li><a href="/autoqa/index.php/autoqa_user/view?id=<?php if (isset($user) && $user != NULL) echo $user->id ?>"><?php if (isset($user) && $user != NULL) echo $user->name ?></a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container">
      <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
          <ul class="nav navbar-right nav-tabs">
            <li role="presentation" <?php if ($sort == NULL || $sort == '' || $sort == 'votes') echo 'class="active"' ?>><a href="/autoqa/index.php/question/search?sort=votes&q=<?php echo $q; ?>">Most Voted</a></li>
            <li role="presentation" <?php if ($sort == 'answers') echo 'class="active"' ?>><a href="/autoqa/index.php/question/search?sort=answers&q=<?php echo $q; ?>">Most Answered</a></li>
            <li role="presentation" <?php if ($sort == 'time') echo 'class="active"' ?>><a href="/autoqa/index.php/question/search?sort=time&q=<?php echo $q; ?>">Newest</a></li>
          </ul>
        </div>
      </div>

      <?php
      if (isset($questions)) {
        foreach ($questions as $question) {
          $answer_count = (isset($question->answers) && $question->answers != NULL) ? count($question->answers) : 0;
          echo '<div class="row collapse-group">'
          . '<div class="col-sm-2 question-sidebar question-sidebar-left">'
          . "<p><span class='label label-default votes' id='votes$question->id'></span></p>"
          . '</div>';
          echo "<div class='col-sm-10'>\n"
          . "<div class='col-sm-10'>\n"
          . "<h3><a href='/autoqa/index.php/question/view?id=$question->id'>$question->question</a></h3>\n"
          . "<p class='question-post-meta category'><a href='/autoqa/index.php/question/search?category_id=$question->category_id'>in $question->category_name</a> <a class='answer-toggle' href='#'>$answer_count Answer(s)</a></p>"
          . "</div>\n"
          . "<div class='col-sm-2 question-sidebar question-post-meta'>\n"
          . "<p><time>$question->time</time></p>"
          . "<p class='no-dec'><a href='/autoqa/index.php/autoqa_user/view?id=$question->user_id'>$question->name</p>\n"
          . "</div>\n";
          if ($answer_count > 0) {
            echo "<div class='col-sm-10 collapse'>\n";
            foreach ($question->answers as $answer) {
              echo "<hr /><div class='row'>\n"
              . '<div class="col-md-2 answer-sidebar">'
              . "<div class='text-center'><div class='rating-container'><input id='rating$answer->id' type='number' class='rating' min='0' max='5' step='1' data-size='xs' data-symbol='&#9733;' data-show-caption='false' data-readonly='true' data-show-clear='false'></div></div>"
              . "</div>\n"
              . "<div class='col-md-6 answer'>\n"
              . "<a href='/autoqa/index.php/question/view?id=$question->id#answer$answer->id'>$answer->answer</a><br />\n"
              . "</div>\n"
              . '<div class="col-md-4 answer-sidebar answer-sidebar-right answer-meta">'
              . "<p><time>$answer->time</time></p>"
              . "<p class='no-dec'><a href='/autoqa/index.php/autoqa_user/view?id=$answer->user_id'>$answer->name</a></p>\n"
              . "</div></div>\n";
            }
            echo "</div>";
          }
          echo "</div></div><hr />\n";
        }
      }
      ?>
    </div>
    <script type="text/javascript">
      $(document).ready(function() {
        $('time').each(function() {
          // Unix timestamp is in seconds. But Date object expect miliseconds.
          // So have to multiply by 1000
          var date = new Date(parseInt($(this).html()) * 1000);
          $(this).html(date.toLocaleTimeString() + ' ' + date.toLocaleDateString());
        });

        $('.rating').each(function() {
          var answerId = $(this).attr('id').replace('rating', '');
          $.ajax({
            url: '/autoqa/index.php/rest/answers/' + answerId + '/ratings',
            success: function(data) {
              var rating = jQuery.parseJSON(data);
              $('#rating' + rating.answer_id).rating('update', rating.rating);
            },
            async: true,
            type: 'GET'
          });
        });

        $('[id^=votes]').each(function() {
          var questionId = $(this).attr('id').replace('votes', '');
          $.ajax({
            url: '/autoqa/index.php/rest/questions/' + questionId + '/votes',
            success: function(data) {
              var result = $.parseJSON(data);
              $('#votes' + result.question_id).html(result.votes);
            },
            async: true,
            type: 'GET'
          });
        });
      });

      $('.answer-toggle').on('click', function(e) {
        e.preventDefault();
        var $this = $(this);
        var $collapse = $this.closest('.collapse-group').find('.collapse');
        $collapse.collapse('toggle');
      });
    </script>
  </body>
</html>