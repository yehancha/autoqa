<html>
  <head>
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-theme.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/autoqa.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/register_view.css'); ?>" />    
    <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
  </head>
  <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/autoqa/">AutoQA</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="/autoqa/">Home</a></li>
            <li><a href="/autoqa/index.php/question/ask">Ask</a></li>
            <li><a href="/autoqa/index.php/category_tag">Categories/Tags</a></li>
          </ul>
          <form class="navbar-form navbar-left" role="search" action="/autoqa/index.php/question/search" method="GET">
            <div class="form-group">
              <input name="q" id="q" type="text" class="form-control" placeholder="Search" size="50">
            </div>
          </form>
          <ul class="nav navbar-nav navbar-right <?php if (isset($user) && $user != NULL) echo 'hidden' ?>">
            <li><a href="/autoqa/index.php/auth/login">Login</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right <?php if (!isset($user) || $user == NULL) echo 'hidden' ?>">
            <li><a href="/autoqa/index.php/auth/logout">Logout</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right <?php if (!isset($user) || $user == NULL) echo 'hidden' ?>">
            <li><a href="/autoqa/index.php/autoqa_user/view?id=<?php if (isset($user) && $user != NULL) echo $user->id ?>"><?php if (isset($user) && $user != NULL) echo $user->name ?></a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container">
      <?php
      if (isset($error)) {
        echo "<div class='alert alert-danger' role='alert'>$error</div>";
      }
      ?>
      <form class="form-register" action="/autoqa/index.php/auth/register" method="POST">
        <h2 class="form-register-heading">Register here</h2>
        <label for="inputName" class="sr-only">Name</label>
        <input type="text" name="name" id="inputName" class="form-control" length="128" placeholder="Name" pattern=".{3,}" required title="Should be at least 3 letters long" autofocus>
        <label for="inputUsername" class="sr-only">Username</label>
        <input type="text" name="username" id="inputUsername" class="form-control" length="32" placeholder="Username" pattern=".{3,}" required title="Should be at least 3 letters long">
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" name="password" id="inputPassword" class="form-control" length="15" placeholder="Password" pattern=".{8,}" onchange="form.inputConfPassword.pattern = this.value;" required title="Should be at least 8 characters long">
        <label for="inputConfPassword" class="sr-only">Confirm Password</label>
        <input type="password" name="conf_password" id="inputConfPassword" class="form-control" length="15" placeholder="Confirm Password" pattern=".{8,}" required title="Passwords don't match">
        <button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
      </form>
    </div>
  </body>
</html>