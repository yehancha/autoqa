<html>
  <head>
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-theme.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/autoqa.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/user_view.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/star-rating.min.css'); ?>" />
    <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/star-rating.min.js') ?>"></script>
  </head>
  <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/autoqa/">AutoQA</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="/autoqa/">Home</a></li>
            <li><a href="/autoqa/index.php/question/ask">Ask</a></li>
            <li><a href="/autoqa/index.php/category_tag">Categories/Tags</a></li>
          </ul>
          <form class="navbar-form navbar-left" role="search" action="/autoqa/index.php/question/search" method="GET">
            <div class="form-group">
              <input name="q" id="q" type="text" class="form-control" placeholder="Search" size="50">
            </div>
          </form>
          <ul class="nav navbar-nav navbar-right <?php if (isset($user) && $user != NULL) echo 'hidden' ?>">
            <li><a href="/autoqa/index.php/auth/login">Login</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right <?php if (!isset($user) || $user == NULL) echo 'hidden' ?>">
            <li><a href="/autoqa/index.php/auth/logout">Logout</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right <?php if (!isset($user) || $user == NULL) echo 'hidden' ?>">
            <li><a href="/autoqa/index.php/autoqa_user/view?id=<?php if (isset($user) && $user != NULL) echo $user->id ?>"><?php if (isset($user) && $user != NULL) echo $user->name ?></a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container">
      <div class="row">
        <div class="col-sm-8">
          <div class="row">
            <div class="col-sm-offset-2">
              <h1>Questions</h1>
            </div>
          </div>
          <?php
          if (isset($user_data->questions)) {
            foreach ($user_data->questions as $question) {
              echo '<div class="row"><hr />'
              . '<div class="col-sm-2 question-sidebar question-sidebar-left">'
              . "<p><span class='label label-default votes'>$question->votes</span></p>"
              . '</div>'
              . "<div class='col-sm-10'>\n"
              . "<h3><a href='/autoqa/index.php/question/view?id=$question->id'>$question->question</a></h3>\n"
              . "<p class='question-post-meta category'><a href='/autoqa/index.php/question/search?category_id=$question->category_id'>in $question->category_name</a> <time>$question->time</time></p>"
              . "</div>\n"
              . "</div>\n";
            }
          }
          ?>

          <div class="row">
            <div class="col-sm-offset-2">
              <h1>Answers</h1>
            </div>
          </div>
          <?php
          if (isset($user_data->answers)) {
            foreach ($user_data->answers as $answer) {
              echo "<div class='row'><hr />\n"
              . '<div class="col-md-2 answer-sidebar">'
              . "<div class='text-center'><div class='rating-container'><input id='rating$answer->id' type='number' class='rating' min='0' max='5' step='1' data-size='xs' data-symbol='&#9733;' data-show-caption='false' data-readonly='true' data-show-clear='false'></div></div>"
              . "</div>\n"
              . "<div class='col-md-10'>\n"
              . "<div class='answer'>\n"
              . "<p><h4><a href='/autoqa/index.php/question/view?id=$answer->question_id#answer$answer->id'>$answer->answer</a><h4></p>\n"
              . "</div>\n"
              . "<div class='answer-meta'>\n"
              . "<p><time>$answer->time</time></p>"
              . "<p class='question'>for <a href='/autoqa/index.php/question/view?id=$answer->question_id'>$answer->question</a></p>\n"
              . "</div>\n"
              . "</div>\n"
              . "</div>\n";
            }
          }
          ?>
        </div>
        
        <div class="col-sm-4">
          <div class="sidebar-module sidebar-module-inset">
            <h2><?php echo $user_data->name ?></h2>
            <p>Username: <?php echo $user_data->username ?></p>
            <p>Reputation: <?php echo round($user_data->reputation) ?></p>
            <p>Questions: <?php echo count($user_data->questions) ?></p>
            <p>Answers: <?php echo count($user_data->answers) ?></p>
          </div>
        </div>
      </div>
    </div>
    <script type="text/javascript">
      $(document).ready(function() {
        $('time').each(function() {
          // Unix timestamp is in seconds. But Date object expect miliseconds.
          // So have to multiply by 1000
          var date = new Date(parseInt($(this).html()) * 1000);
          $(this).html(date.toLocaleTimeString() + ' ' + date.toLocaleDateString());
        });

        $('.rating').each(function() {
          var answerId = $(this).attr('id').replace('rating', '');
          $.ajax({
            url: '/autoqa/index.php/rest/answers/' + answerId + '/ratings',
            success: function(data) {
              var rating = jQuery.parseJSON(data);
              $('#rating' + rating.answer_id).rating('update', rating.rating);
            },
            async: true,
            type: 'GET'
          });
        });
      });
    </script>
  </body>
</html>