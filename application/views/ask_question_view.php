<html>
  <head>
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-theme.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/autoqa.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/login_view.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/ask-question-view.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-tagsinput.css'); ?>" />
    <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap-tagsinput.js') ?>"></script>
  </head>
  <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/autoqa/">AutoQA</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="/autoqa/">Home</a></li>
            <?php
            $is_new_question = (!isset($question) || $question == NULL);
            // We have to mark this item as active. But if there is a question
            //  coming preloaded, the intention is to edit the question.
            // So we mark this link as active only if there is no question
            // coming in thus a fresh question.
            ?>
            <li <?php if ($is_new_question) echo 'class="active"'; ?>><a href="/autoqa/index.php/question/ask">Ask</a></li>
            <li><a href="/autoqa/index.php/category_tag">Categories/Tags</a></li>
          </ul>
          <form class="navbar-form navbar-left" role="search" action="/autoqa/index.php/question/search" method="GET">
            <div class="form-group">
              <input name="q" id="q" type="text" class="form-control" placeholder="Search" size="50">
            </div>
          </form>
          <ul class="nav navbar-nav navbar-right <?php if (isset($user) && $user != NULL) echo 'hidden' ?>">
            <li><a href="/autoqa/index.php/auth/login">Login</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right <?php if (!isset($user) || $user == NULL) echo 'hidden' ?>">
            <li><a href="/autoqa/index.php/auth/logout">Logout</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right <?php if (!isset($user) || $user == NULL) echo 'hidden' ?>">
            <li><a href="/autoqa/index.php/autoqa_user/view?id=<?php if (isset($user) && $user != NULL) echo $user->id ?>"><?php if (isset($user) && $user != NULL) echo $user->name ?></a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <?php
    // We just create and empty form in the ahppy day sinario. But if there is
    // a question coming in, the intention is to edit the question. So in that
    // case we set appropriate data along the way.
    ?>
    <div class="container">
      <form class="form-ask" action="/autoqa/index.php/question/<?php
      if ($is_new_question)
        echo 'ask';
      else
        echo 'update';
      ?>" method="POST">
        <h2 class="form-ask-heading"><?php
          if ($is_new_question) {
            echo 'Ask question';
          } else {
            echo 'Edit question';
          }
          ?></h2>
        <div class="form-group">
          <label for="inputQuestion" class="sr-only">Question</label>
          <input type="text" name="question" id="inputQuestion" class="form-control" placeholder="Question" <?php if (!$is_new_question) echo "value='$question->question'" ?> required />
        </div>
        <div class="form-group">
          <label for="textareaDescription" class="sr-only">Description</label>
          <textarea name="description" id="textareaDescription" class="form-control" rows="10" placeholder="Description" required ><?php if (!$is_new_question) echo $question->description ?></textarea>
        </div>
        <div class="form-group">
          <label for="selectCategory">Category</label>
          <select name="category" id="selectCategory" class="form-control" ><?php
            foreach ($categories as $category) {
              $id = $category->id;
              $name = $category->name;
              echo "\n<option value='$id' ";
              if (!$is_new_question && $id == $question->category_id)
                echo 'selected';
              echo ">$name</option>";
            }
            ?>
          </select>
        </div>
        <div class="form-group">
          <label for="selectTags">Tags</label><br />
          <select multiple name="tag[]" id="selectTags" class="form-control" data-confirm-keys="[13]" data-role="tagsinput" placeholder="Type here"><?php
            if (!$is_new_question) {
              foreach ($question->tags as $tag) {
                echo "\n<option value='$tag'>$tag</option>";
              }
            }
            ?>
          </select>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit"><?php
          if ($is_new_question)
            echo 'Ask!';
          else
            echo 'Save';
          ?></button>
        <?php if (!$is_new_question) echo "<input name='id' hidden value='$question->id'>"; ?>
      </form>
    </div>
  </body>
</html>