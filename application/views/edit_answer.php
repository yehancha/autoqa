<html>
  <head>
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-theme.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/autoqa.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/register_view.css'); ?>" />    
    <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
  </head>
  <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/autoqa/">AutoQA</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="/autoqa/">Home</a></li>
            <li><a href="/autoqa/index.php/question/ask">Ask</a></li>
            <li><a href="/autoqa/index.php/category_tag">Categories/Tags</a></li>
          </ul>
          <form class="navbar-form navbar-left" role="search" action="/autoqa/index.php/question/search" method="GET">
            <div class="form-group">
              <input name="q" id="q" type="text" class="form-control" placeholder="Search" size="50">
            </div>
          </form>
          <ul class="nav navbar-nav navbar-right <?php if (isset($user) && $user != NULL) echo 'hidden' ?>">
            <li><a href="/autoqa/index.php/auth/login">Login</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right <?php if (!isset($user) || $user == NULL) echo 'hidden' ?>">
            <li><a href="/autoqa/index.php/auth/logout">Logout</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right <?php if (!isset($user) || $user == NULL) echo 'hidden' ?>">
            <li><a href="/autoqa/index.php/autoqa_user/view?id=<?php if (isset($user) && $user != NULL) echo $user->id ?>"><?php if (isset($user) && $user != NULL) echo $user->name ?></a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container">
      <div class="row"><div class="col-sm-8 col-sm-offset-2">
          <form class="form-ask" action="/autoqa/index.php/answer/update" method="POST">
            <h2 class="form-register-heading">Edit your answer</h2>
            <div class="form-group">
              <label for="textareaAnswer" class="sr-only">Answer</label>
              <textarea name="answer" id="textareaAnswer" rows="10" class="form-control" placeholder="Type your updated answer here!" required><?php echo $answer->answer ?></textarea><br />
            </div>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Save</button>
            <input name='id' hidden value='<?php echo $answer->id ?>'>
          </form>
        </div></div>
    </div>
  </body>
</html>