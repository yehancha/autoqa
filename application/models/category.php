<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of category
 *
 * @author Yehan
 */
class Category extends CI_Model {
  function __construct() {
    parent::__construct();
  }
  
  /**
   * Get all the categories
   * @return Category array
   */
  function get_all() {
    $this->db->select('category.*, (SELECT COUNT(id) FROM question WHERE question.category_id=category.id) AS count');
    $this->db->order_by('name');
    $query = $this->db->get('category');
    $categories = array();
    foreach ($query->result() as $row) {
      $categories[] = $row;
    }
    return $categories;
  }
}

?>
