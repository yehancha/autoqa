<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of tag
 *
 * @author Yehan
 */
class Tag extends CI_Model {

  function __construct() {
    parent::__construct();
  }

  /**
   * Get all the tags
   * @return tag array
   */
  function get_all() {
    $this->db->select('tag, COUNT(tag) AS count');
    $this->db->group_by('tag');
    $this->db->order_by('count DESC');
    $query = $this->db->get('tagged_to');
    $tags = array();
    foreach ($query->result() as $row) {
      $tags[] = $row;
    }
    return $tags;
  }
  
  /**
   * Add a given tags to a given question
   * @param type $question_id Id of the question the tag needs to be added
   * @param type $tags Actual tags
   */
  function add($question_id, $tags) {
    if ($tags == null || count($tags) == 0) {
      return;
    }
    
    // normalizing
    $tags = array_map('strtolower', $tags); // lower casing
    $tags = array_map('trim', $tags); // stripping whitespace from the beginnings and ends
    $tags = str_replace(' ', '-', $tags); // replacing sapces with '-'
    $tags = array_unique($tags); // removing duplicates
    
    $tagged = array();
    foreach ($tags as $tag) {
      $tagged[] = array (
          'question_id' => $question_id,
          'tag' => $tag
      );
    }
    $this->db->insert_batch('tagged_to', $tagged);
  }
  
  /**
   * Removes all the current tags of the given question and adds new tags
   * @param type $question_id Id of the question which the tags needed to be updated
   * @param type $tags new tags
   */
  function update($question_id, $tags) {
    $this->db->delete('tagged_to', array('question_id' => $question_id));
    $this->add($question_id, $tags);
  }
  
  /**
   * Get all the tags tp a given question
   * @param type $question_id Id of the question the tags are needed for
   * @return tag array
   */
  function get_tags_to_question($question_id) {
    $this->db->select('tag');
    $query = $this->db->get_where('tagged_to', array('question_id' => $question_id));
    $tags = array();
    foreach ($query->result() as $row) {
      $tags[] = $row->tag;
    }
    return $tags;
  }

}

?>
