<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of user
 *
 * @author yehancha
 */
class User extends CI_Model {

  /**
   * Constructor: loads session library and URL helper
   */
  function __construct() {
    parent::__construct();
    $this->load->library('session');
    $this->load->helper('url');
  }

  /**
   * Registers a new user
   * @param type $name Full Name
   * @param type $username username
   * @param type $password password
   * @return Error if any
   */
  public function register($name, $username, $password) {
    $result = new stdClass();
    // is username unique?
    $res = $this->db->get_where('user', array('username' => $username));
    if ($res->num_rows() > 0) {
      $result->error = "Username '$username' already exists. Please try another username.";
      return $result;
    }

    // username is unique
    $hashpassword = sha1($password);
    $data = array('name' => $name, 'username' => $username, 'password' => $hashpassword);
    $this->db->insert('user', $data);

    return $result; // no error message because all is ok
  }

  /**
   * Logs in the user if the credentials are correct
   * @param type $username username
   * @param type $pwd password
   * @return Error if any
   */
  public function login($username, $pwd) {
    $this->db->where(array('username' => $username, 'password' => sha1($pwd)));
    $query = $this->db->get('user', array('id'));
    $result = new stdClass();
    if ($query->num_rows() != 1) { // should be only ONE matching row!!
      $result->error = 'Username or password is wrong!';
      return $result;
    }

    // Check whether an user is already logged in.
    // If an user is logged in and if that user is not the same user
    // who is trying to log in now, we log out the old user.
    $user = $this->get_current_user();
    if ($user != NULL && $user->username != $username) {
      $this->logout();
      $user = NULL;
    }

    // So passing above step, if there is a user who is still logged in, that
    // user must be the same user who is trying to log in now. So we don't have
    // to do anything.
    // We do log in if there is no users logged in.    
    if ($user == NULL) {
      $session_id = $this->session->userdata('session_id');
      $row = $query->row_array();
      $this->db->insert('login', array('user_id' => $row['id'], 'session_id' => $session_id));
    }

    return $result;
  }

  /**
   * Logs out the current user
   */
  public function logout() {
    $this->db->delete('login', array('session_id' => $this->session->userdata('session_id')));
  }

  /**
   * Checks whether a user is logged in for the current session
   * @return boolean TRUE if there is a logged in user, FALSE else.
   */
  public function is_logged_in() {
    $session_id = $this->session->userdata('session_id');
    $res = $this->db->get_where('login', array('session_id' => $session_id));
    if ($res->num_rows() == 1) {
      return TRUE;
    } else {
      return FALSE;
    }
  }

  /**
   * Set a redirect URL for current user
   * @param type $url URL to be set
   */
  public function set_redirect_url($url) {
    $this->session->set_userdata('redirect_url', $url);
  }

  /**
   * Get the redirect URL if any, for the current user
   * @return type Redirect URL
   */
  public function get_redirect_url() {
    $url = $this->session->userdata('redirect_url');
    $this->session->unset_userdata('redirect_url');
    return $url;
  }

  /**
   * Get the current logged in user
   * @return type Logged in user
   */
  public function get_current_user() {
    $session_id = $this->session->userdata('session_id');
    $this->db->select('user_id');
    $query = $this->db->get_where('login', array('session_id' => $session_id));

    $result = NULL;
    if ($query->num_rows() == 1) {
      $this->db->select('id, type, name, username');
      $query = $this->db->get_where('user', array('id' => $query->row()->user_id));
      if ($query->num_rows() == 1) {
        $result = $query->row();
      }
    }
    return $result;
  }

  /**
   * Get the given user
   * @param type $id Id of the user to be returned
   * @return type User
   */
  public function get($id) {
    $this->db->select('id, username, name');
    $user = $this->db->get_where('user', array('id' => $id))->row();

    if (!isset($user) || $user == NULL) {
      $user->error = 'USER_ERROR';
      return $user;
    }

    $this->load->model('question_model', '', TRUE);
    $user->questions = $this->question_model->get_for_user($id);

    $this->load->model('answer_model', '', TRUE);
    $user->answers = $this->answer_model->get_for_user($id);

    $user->reputation = $this->calculate_reputation($user->questions, $user->answers);
    
    return $user;
  }

  /**
   * Calculate the reputation using given sets of questions and answers
   * @param type $questions questions
   * @param type $answers answers
   * @return double reputation
   */
  private function calculate_reputation($questions, $answers) {
    $reputation = 0;

    // Reputation for asking questions: 1/Question
    if (isset($questions) && $questions != NULL) {
      $reputation += count($questions);

      // Reputation for votes: 5/vote
      foreach ($questions as $question) {
        $reputation += $question->votes * 5;
      }
    }

    // Reputation for answering: 2/Answer
    if (isset($answers) && $answers != NULL) {
      $reputation += count($answers) * 2;
      
      // Reputation for ratings: rating * 10
      foreach ($answers as $answer) {
        $reputation += $answer->rating * 10;
      }
    }
    
    return $reputation;
  }

}
