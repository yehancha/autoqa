<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of answer_model
 *
 * @author Yehan
 */
class Answer_model extends CI_Model {

  var $answer;
  var $time;
  var $user_id;
  var $question_id;

  function __construct() {
    parent::__construct();
  }

  /**
   * Get the answer for the given id.
   * @param type $id Id of the answer to be returned.
   * @return Answer
   */
  function get($id) {
    $query = $this->db->get_where('answer', array('id' => $id));
    return $query->row();
  }

  /**
   * Adds a given answer to a given question.
   * @param type $answer Answer to be added
   * @param type $question_id Id of the question the answer to be added
   * @param type $user current user
   * @return Id of the newly added answer 
   */
  function add($answer, $question_id, $user) {
    $this->user_id = $user->id;
    $this->answer = $answer;
    $this->question_id = $question_id;
    $this->time = time();

    $this->db->insert('answer', $this);

    return $this->db->insert_id();
  }

  /**
   * Updates a given answer.
   * @param type $id Id of the answer to be updated
   * @param type $answer Actual answer
   * @param type $question_id This is not used
   * @param type $user current user
   * @return Error if any OR question_id of the question which the answer bemongs to
   */
  function update($id, $answer, $question_id, $user) {
    $this->db->select('user_id, question_id');
    $query = $this->db->get_where('answer', array('id' => $id));
    $user_id = $query->row()->user_id;
    $question_id = $query->row()->question_id;

    $result = new stdClass();
    if (!isset($user_id) || $user_id != $user->id) {
      $result->error = '<h1>Oops!</h1><p>You are not allowed to edit this Answer.</p><p>Please click <a href="javascript:history.back()">here</a> to go back to where you were OR press the back button of your browser</p>';
      return $result;
    }

    $this->db->where('id', $id);
    $this->db->update('answer', array('answer' => $answer, 'time' => time()));

    $result->question_id = $question_id;
    return $result;
  }

  /**
   * Get the ratings for a given answer
   * @param type $answer_id Id of the answer the rating is needed.
   * @return {rating: overall rating, user_rating: current users rating for this answer}
   */
  function get_ratings($answer_id) {
    $this->db->select_avg('rating');
    $this->db->where('answer_id', $answer_id);
    $this->db->from('rate');
    $query = $this->db->get();
    $rating = new stdClass();
    $rating->answer_id = $answer_id;
    if ($query->num_rows() > 0) {
      $rating->rating = $query->row()->rating;
    }
    $this->load->model('user', '', TRUE);
    $user = $this->user->get_current_user();
    if ($user != NULL) {
      $user_id = $user->id;
      $this->db->select('rating');
      $query = $this->db->get_where('rate', array('user_id' => $user_id, 'answer_id' => $answer_id));
      if ($query->num_rows() > 0) {
        $rating->user_rating = $query->row()->rating;
      }
    }

    return $rating;
  }

  /**
   * Rate a given answer
   * @param type $answer_id Id of the answer to be rated
   * @param type $rating  Ratings to be added
   * @return overall rating
   */
  function rate($answer_id, $rating) {
    $result = new stdClass();

    $this->load->model('user', '', TRUE);
    $user = $this->user->get_current_user();
    if ($user == NULL) {
      $result = $this->get_ratings($answer_id);
      $result->error = 'You are not logged in';
      return $result;
    }

    // Normalizing
    if ($rating > 5) {
      $rating = 5;
    } else if ($rating < 1) {
      $rating = 1;
    }

    $user_id = $user->id;
    $query = $this->db->get_where('rate', array('user_id' => $user_id, 'answer_id' => $answer_id));
    if ($query->num_rows == 0) {
      // User has not rate this answer before
      $this->db->insert('rate', array('user_id' => $user_id, 'answer_id' => $answer_id, 'rating' => $rating));
    } else {
      // User has rate this answer before. So we update
      $this->db->where('answer_id', $answer_id);
      $this->db->where('user_id', $user_id);
      $this->db->update('rate', array('rating' => $rating));
    }

    $result = $this->get_ratings($answer_id);
    return $result;
  }

  /**
   * Removes the current user's rating from the given answer
   * @param type $answer_id Id of the answer the rating should be removed
   * @return Overall rating
   */
  function clear_rating($answer_id) {
    $result = $this->get_ratings($answer_id);
    $user = $this->user->get_current_user();
    if ($user == NULL) {
      $result->error = 'You are not logged in';
      return $result;
    }

    $user_id = $user->id;
    $this->db->delete('rate', array('user_id' => $user_id, 'answer_id' => $answer_id));
    if ($this->db->affected_rows() == 0) {
      // If now rows affected, the use had not voted
      $result->error = 'You have not rated';
    } else {
      $result = $this->get_ratings($answer_id);
    }

    return $result;
  }

  /**
   * Get all the question which are owned by the given user
   * @param type $user_id Id of the user which the answers are needed for
   * @return Answer array
   */
  function get_for_user($user_id) {
    $this->db->select('answer.*, (SELECT AVG(rating) FROM rate WHERE answer_id=answer.id) AS rating, question.id AS question_id, question.question');
    $this->db->from('answer');
    $this->db->join('rate', 'answer_id=answer.id');
    $this->db->join('question', 'question.id=answer.question_id');
    $this->db->where('answer.user_id', $user_id);
    $this->db->order_by('rating DESC');
    $this->db->order_by('time DESC');
    $query = $this->db->get();

    $answers = array();
    foreach ($query->result() as $row) {
      $answers[] = $row;
    }

    return $answers;
  }

  /**
   * Deletes the answer for the given answer id.
   * @param type $id Id of the answer to be deleted
   * @return the id of the question which belonged the question deleted
   */
  function delete($id) {
    $this->load->model('user', '', TRUE);
    $user = $this->user->get_current_user();

    $result = new stdClass();

    if (!isset($user) || $user == NULL) {
      $result->error = 'You are not authorised to delete this Answer';
      return $result;
    }

    $delete_allowed = $user->type == 'admin';
    if (!$delete_allowed) {
      $this->db->select('user_id, question_id');
      $row = $this->db->get_where('answer', array('id' => $id))->row();
      $user_id = NULL;
      if (isset($row) && $row != NULL) {
        $user_id = $row->user_id;
        $result->question_id = $row->question_id;
      } else {
        $result->error = 'Cannot find the Answer you are trying to delete.';
        return $result;
      }
      $delete_allowed = $user->id == $user_id;
    } else {
      $this->db->select('question_id');
      $result->question_id = $this->db->get_where('answer', array('id' => $id))->row()->question_id;
    }

    if (!$delete_allowed) {
      $result->error = 'You are not authorised to delete this Answer';
      return $result;
    }

    $this->db->delete('answer', array('id' => $id));
    // We don't need to think about deleting ratings since database
    // relations take care of that.

    return $result;
  }

}

?>
