<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of question
 *
 * @author Yehan
 */
class Question_model extends CI_Model {

  var $question;
  var $description;
  var $category_id;
  var $time;
  var $user_id;

  /**
   * Constructor: loads user model
   */
  function __construct() {
    parent::__construct();
    $this->load->model('user');
  }

  /**
   * Adds a new question as the current user
   * @param type $question Actual question
   * @param type $description Question description
   * @param type $category_id Id of the category the question needs to be put in
   * @param type $tags Tags to be added
   * @return Id of the newly added question
   */
  function add($question, $description, $category_id, $tags) {
    $this->question = $question;
    $this->description = $description;
    $this->category_id = $category_id;
    $this->time = time();
    $this->user_id = $this->user->get_current_user()->id;

    $this->db->insert('question', $this);

    // Adding tags
    $question_id = $this->db->insert_id();
    $this->load->model('tag', '', TRUE);
    $this->tag->add($question_id, $tags);

    return $question_id;
  }

  /**
   * Get the question for the given id
   * @param type $id Id of the question to be returned
   * @param type $replace_new_line Whether to replace the new line character with
   *                              with html <![CDATA[<br />]]>. Default TRUE
   * @param type $with_answers Whether the answers are needed. Default TRUE
   * @return question
   */
  function get($id, $replace_new_line = TRUE, $with_answers = TRUE) {
    $this->db->select('question.id, question, description, time, category_id, user_id, category.name AS category_name, user.name AS user_name');
    $this->db->from('question');
    $this->db->join('category', 'category.id=question.category_id');
    $this->db->join('user', 'user.id=question.user_id');
    $this->db->where('question.id', $id);
    $query = $this->db->get();

    if ($query->num_rows() == 0) {
      return;
    }

    // make the description html friendly
    $question = $query->row();
    if ($replace_new_line)
      $question->description = str_replace("\n", '<br />', $question->description);

    // Get related tags
    $this->db->select('tag');
    $this->db->from('tagged_to');
    $this->db->where('question_id', $id);
    $query = $this->db->get();

    $tags = array();
    foreach ($query->result() as $row) {
      $tags[] = $row->tag;
    }
    $question->tags = $tags;

    // If only the question is requested, we supply only upto this
    if (!$with_answers) {
      return $question;
    }

    // Get answers for the question
    $this->db->select('answer.id, answer, time, user_id, user.name AS user_name, (SELECT AVG(rating) FROM rate where rate.answer_id=answer.id) AS rating');
    $this->db->from('answer');
    $this->db->join('user', 'user.id=answer.user_id');
    $this->db->where('answer.question_id', $id);
    $this->db->order_by('rating', 'desc');
    $query = $this->db->get();

    $answers = array();
    foreach ($query->result() as $row) {
      if ($replace_new_line)
        $row->answer = str_replace("\n", '<br />', $row->answer); // make the answer html friendly
      $answers[] = $row;
    }
    $question->answers = $answers;

    $this->load->model('user', '', TRUE);
    $user = $this->user->get_current_user();
    if ($user != NULL) {
      $query = $this->db->get_where('vote', array('user_id' => $user->id, 'question_id' => $id));
      if ($query->num_rows() == 1) {
        // User has voted
        $question->has_voted = TRUE;
      } else {
        $question->has_voted = FALSE;
      }
    }

    $question->id = $id;

    return $question;
  }

  /**
   * Updates a given question
   * @param type $id Id of the question to be updated
   * @param type $question Actual question
   * @param type $description Question description
   * @param type $category_id Id of the category the question needs to be put in
   * @param type $tags Tags to be added
   * @param type $user current user
   * @return Error if any
   */
  function update($id, $question, $description, $category_id, $tags, $user) {
    $this->db->select('user_id');
    $query = $this->db->get_where('question', array('id' => $id));
    $user_id = $query->row()->user_id;
    $result = new stdClass();

    if (!isset($user_id) || $user_id != $user->id) {
      $result->error = '<h1>Oops!</h1><p>You are not allowed to edit this Question.</p><p>Please click <a href="javascript:history.back()">here</a> to go back to where you were OR press the back button of your browser</p>';
      return $result;
    }

    $this->db->where('id', $id);
    $this->db->update('question', array(
        'question' => $question,
        'description' => $description,
        'category_id' => $category_id,
        'time' => time()
    ));

    // Have to update tags
    $this->load->model('tag', '', TRUE);
    $this->tag->update($id, $tags);
  }

  /**
   * Get votes for the given question
   * @param type $question_id Id of the question the votes are needed for
   * @return {votes:, question_id:}
   */
  function get_votes($question_id) {
    $this->db->where('question_id', $question_id);
    $this->db->from('vote');
    $result = new stdClass();
    $result->votes = $this->db->count_all_results();
    $result->question_id = $question_id;
    return $result;
  }

  /**
   * Add a vote for a given question after authenticating the user
   * @param type $question_id Id of the question to be voted
   * @return Error if any OR 1
   */
  function vote($question_id) {
    $user = $this->user->get_current_user();
    if ($user == NULL) {
      return 'You are not logged in';
    }

    $user_id = $user->id;
    $query = $this->db->get_where('vote', array('user_id' => $user_id, 'question_id' => $question_id));
    if ($query->num_rows > 0) {
      // This user has already voted
      return 'You have voted already';
    }

    $this->db->insert('vote', array('user_id' => $user_id, 'question_id' => $question_id));
    return 1;
  }

  /**
   * Removes a vote from the given question after authenticating the user
   * @param type $question_id Id of the question the vote needs to be removed
   * @return Error if any OR 1
   */
  function clear_vote($question_id) {
    $user = $this->user->get_current_user();
    if ($user == NULL) {
      return 'You are not logged in';
    }

    $user_id = $user->id;
    $this->db->delete('vote', array('user_id' => $user_id, 'question_id' => $question_id));
    if ($this->db->affected_rows() == 0) {
      // If now rows affected, the use had not voted
      return 'You have not voted';
    } else {
      return 1;
    }
  }

  /**
   * Saerch for a question on given params
   * @param type $term Search term which is to be compared against the questions
   * @param type $sort How the sorting should be. Default: votes
   * @param type $category_id Filters by a category. Default: NULL
   * @return question array
   */
  function search($term, $sort = 'votes', $category_id = NULL) {
    $words = explode(' ', $term);
    $word_count = count($words);

    $generics = array();
    $tags = array();
    foreach ($words as $word) {
      if (strpos($word, 'tag=') !== FALSE) { // if there is a position, there is a substring
        $tags[] = str_replace('tag=', '', $word);
      } else {
        $generics[] = $word;
      }
    }
    $tag_count = count($tags);

    // First we create the inner SELECT query. This is needed if we are searching for
    // tags too. It'll be like:
    // SELECT * FROM (SELECT this, that FROM this join that on this=that where this=that) inner WHERE this=that
    $this->db->select('question.id, question, user_id, user.name, time, category.id AS category_id, category.name AS category_name, (SELECT COUNT(question_id) FROM vote WHERE question_id=question.id) AS vote, (SELECT COUNT(question_id) FROM answer WHERE question_id=question.id) AS answer_count');
    // If there are tags, we count tags to use in the where clouse later
    if ($tag_count > 0) {
      $where_clouse = "(tag='$tags[0]'";
      for ($i = 1; $i < $tag_count; $i++) {
        $where_clouse .= " OR tag='$tags[$i]'";
      }
      $where_clouse .= ')';
      $this->db->select("(SELECT COUNT(tag) FROM tagged_to WHERE question_id=question.id AND $where_clouse) AS tag_count");
    }
    $this->db->from('question');
    $this->db->join('user', 'user.id=question.user_id');
    $this->db->join('category', 'category.id=question.category_id');

    foreach ($generics as $generic) {
      $this->db->or_like('question', $generic);
    }

    if ($category_id != NULL) {
      $this->db->where('category.id', $category_id);
    }

    if ($sort == 'answers') {
      $this->db->order_by('answer_count DESC');
      $this->db->order_by('vote DESC');
      $this->db->order_by('time DESC');
    } else if ($sort == 'time') {
      $this->db->order_by('time DESC');
      $this->db->order_by('vote DESC');
      $this->db->order_by('answer_count DESC');
    } else {
      $this->db->order_by('vote DESC');
      $this->db->order_by('answer_count DESC');
      $this->db->order_by('time DESC');
    }

    // We create the query. If we are searching for tags, we have to nest it.
    // Otherwise we can use it.
    if ($tag_count > 0) {
      $nested_select = $this->db->_compile_select();
      $this->db->_reset_select();
      $this->db->select("* FROM ($nested_select) question", FALSE);
      $this->db->where('tag_count > 0');
    }
    $query = $this->db->get();

    $results = array();
    foreach ($query->result() as $row) {
      $result = $row;

      // We are using a nested SELECT here too
      // Nothing to worry about sql injection since the sub query is static.
      $this->db->select('answer.id, answer,  user_id, name, time, (SELECT AVG(rating) FROM rate where rate.answer_id=answer.id) AS rating');
      $this->db->from('answer');
      $this->db->join('user', 'user.id=answer.user_id');
      $this->db->where('answer.question_id', $result->id);
      $this->db->order_by('rating', 'desc');
      $answers = $this->db->get();
      // Injecting answers to relevent question
      if ($answers->num_rows() > 0) {
        $result->answers = $answers->result();
      }
    }
    return $query->result();
  }

  /**
   * Get the questions owned by a given user
   * @param type $user_id Id of the user the questions are needed for
   * @return Question array
   */
  function get_for_user($user_id) {
    $this->db->select('question.*, category.name AS category_name, (SELECT COUNT(question_id) FROM vote WHERE vote.question_id=question.id) AS votes');
    $this->db->from('question');
    $this->db->join('category', 'category.id=question.category_id');
    $this->db->where('user_id', $user_id);
    $this->db->order_by('votes DESC');
    $this->db->order_by('time DESC');
    $query = $this->db->get();

    $questions = array();
    foreach ($query->result() as $row) {
      $questions[] = $row;
    }

    return $questions;
  }

  /**
   * Deletes the question for the given question id. All the answers and votes
   * of the particular quesion will also be deleted.
   * @param type $id Id of the question to be  deleted
   */
  function delete($id) {
    $this->load->model('user', '', TRUE);
    $user = $this->user->get_current_user();

    $result = new stdClass();

    if (!isset($user) || $user == NULL) {
      return 'You are not authorised to delete this question';
    }

    $delete_allowed = $user->type == 'admin';
    if (!$delete_allowed) {
      $this->db->select('user_id');
      $user_id = $this->db->get_where('question', array('id' => $id))->row()->user_id;
      $delete_allowed = $user->id == $user_id;
    }

    if (!$delete_allowed) {
      return 'You are not authorised to delete this question';
    }

    $this->db->delete('question', array('id' => $id));
    // We don't need to think about deleting answers and votes since database
    // relations take care of that.
  }

}

?>
