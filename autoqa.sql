-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 08, 2015 at 09:22 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `autoqa`
--

CREATE SCHEMA `autoqa`;
USE `autoqa`;

-- --------------------------------------------------------

--
-- Table structure for table `answer`
--

CREATE TABLE IF NOT EXISTS `answer` (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `answer` text NOT NULL,
  `time` int(10) NOT NULL,
  `user_id` int(16) NOT NULL,
  `question_id` int(16) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `question_id` (`question_id`),
  KEY `user_id` (`user_id`),
  KEY `user_id_2` (`user_id`),
  KEY `question_id_2` (`question_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `answer`
--

INSERT INTO `answer` (`id`, `answer`, `time`, `user_id`, `question_id`) VALUES
(1, 'The most common reason for this is getting skin oil onto the bulb during changing of the bulb. This will kill most every bulb on the market today. This creates hot spots on the bulb which will cause the filaments to burn out at an accelerated rate.\r\n\r\nIf you aren''t already, use clean, new, nitrile gloves to keep the oil off. If you don''t have nitrile gloves, you can utilize a paper towel to hold the bulb while putting it into the socket.\r\n\r\nAny kind of contaminants on the bulb will cause the same problem. If there is any kind of leakage getting into your headlight housing, it too can cause problems with your new bulbs. Mainly, if you see condensation on the inside of the housing, it could be the issue.', 1420238887, 2, 1),
(2, 'Another surprisingly common problem is water in the headlight lenses (ie uin the unit iteslf), especially after heavy rainfall. It forms a puddle at the bottom of the unit until maybe you hit a bump or brake hard, then splashes up & hits the bulb itself .. zap! temperature difference upsets the bulb.\r\n\r\nHave a close look at the light units from the outside and see if you can see any water collected at the bottm. might help to wobble the car about a bit.', 1420239020, 3, 1),
(3, 'The clutches on all cars will vary in ''stiffness'' as any particular model of vehicle will have variations of engine size and performance. Corsa and Fiesta are smaller cars and a light clutch is appreciated by a host of people, especially the girls. There are no real problems with the clutches on the cars you mentioned, including Renault, and the issues you outline are either based on limited experiance of the vehicles you mention, or are a hangover from your previous cars clutch.', 1420239047, 3, 2),
(4, 'Any internal combustion engine needs three things to run: fuel; air; ignition source (spark). Almost always, air can be assumed as being present. You should check the other two things, those being the spark and the fuel.\r\n\r\nRemove the spark plug wire from the spark plug and check to see if you are getting spark through the wire. You should see a spark coming from the wire if it is held close to the head of the engine, then spin the engine over (two person operation). Be careful because you can get shocked doing this if not careful (though if you are shocked, it won''t harm you, but you will definitely know there is spark).\r\n\r\nIf you get spark there, check the spark plug as well by pulling the plug, connect it back into the spark plug wire, ground the plug at the head and rotate the engine. You should see a spark over the gap. If you don''t, it''s probably the spark plug.\r\n\r\nRemove the spark plug and smell it to see if you can smell gas on it. If it''s there, you know you are getting fuel. It could be that the carb isn''t doing it''s thing correctly. Too much or too little fuel will cause you issues. Running ethanol laced fuels does not help this, either. The ethanol tends to gum up the carb.', 1420239349, 4, 3),
(5, 'Well, if you''re not getting any compression, that explains why it''s not running - you just need to work out what''s causing the lack of compression.\r\n\r\nIf you''ve already replaced the top end and head gasket, the next suspect would be the piston rings letting the gases into the crankcase - though this usually results in poor compression rather than no compression at all.\r\n\r\nOne trick I have heard to test this (although I''ve not tried it...) is to pour a small amount of diesel into the cylinder, and watch to see how quickly it drains out - if it is quick, the rings have failed. Don''t try and start it while the liquid fuel remains in the cylinder though - if it doesn''t all drain, siphon it out!', 1420239486, 5, 3),
(6, 'Two of the useful features of this setup (I have no evidence to prove they were the design reasons) are:\r\n\r\nwhen braking in a hurry stamping down until you reach the bottom will leave you in first, NOT neutral. This is much safer in many respects than being left with no power in an emergency situation.\r\n\r\nwhen starting from neutral, there is no risk of ending up in the wrong gear; 1 kick down leaves you in first gear. I have ridden very old bikes where neutral was the bottom gear, and sometimes the first click up would leave me in second - where I would stall, not being prepared for this.\r\n\r\nI have also ridden a bike where the gears were the other way round, with 1st at the top, then neutral, then 2nd, 3rd etc - kicking down to change up a gear...less natural...very odd when accelerating hard', 1420239535, 5, 4),
(7, 'Its worth noting that neutral is usually half way between 1st and 2nd, so that a shift from 1st to 2nd feels natural  the same distance as any other upshift. This can have the side-effect of making neutral hard to select on some bikes.\r\n\r\nThe primary reason for this is that it is far more common to desire 1st gear than neutral. If you stop at the lights, you can declutch and stamp all the way down on the gear selector, and be sure youre in 1st, ready to pull away when the light goes green. Accidentally selecting neutral in this situation would be an inconvenience at best, and at worst could lead to falling over when you try to pull away and find out youre in neutral!', 1420239630, 1, 4),
(8, 'The cause of a fuse blowing is always because too high a current is passing through it, so you need to find out why.\r\n\r\nRealistically, options are:\r\n\r\nA short circuit in the wiring to the wiper motor - which you can diagnose with a multimeter\r\n\r\nA failure in the wiper motor itself - best tested (by a non-mechanic) by replacing with a know good one', 1420239662, 1, 5),
(9, 'There isn''t a technical reason. The pattern can be, and sometimes is, quite different than what you state. It is the combination of history and evolving ergonomic considerations in play here. Really old bikes had an all-up pattern so neutral was at the bottom or top of the pattern.\r\n\r\nThere was a government rule established in the US 40 years ago standardizing motorcycle controls for regulated motorcycles, so there are political reasons as well.\r\n\r\nBut it isn''t for technical reasons.\r\n\r\nBetween first and second is the most logical place to put neutral given that it can easily placed anywhere. Don''t forget, you aren''t spending time in the neutral position in ordinary driving, so you certainly don''t want to find yourself there inadvertently!', 1420239862, 2, 4),
(10, 'This seems to be a faulty thermostat which is stuck partially open. Also check your coolant level on a cold engine. Use the marks on the expansion tank but do exceed the maximum mark.', 1420239887, 2, 6),
(11, 'Your radiator, if original, is 11 years old and may be full of scale depending on water quality in your area. It may need replacing/rodding out/recoring. Also, check that the bottom hose you purchased has spring inside to prevent collapse. I always use quality premixed coolant so water quality is not an issue. N.B. some people will tell you to discard thermostat. BAD idea. Had the exact same problem with my V8 shorty 40 and above solutions resolved problem. Sitting on 110kph with 38c. outside temp. temp. gauge hovers around 85c.', 1420240030, 3, 6),
(12, 'Fuel filler neck or pipe, it connects to a rubber hose (8) that connects to the tank (1). Hose clamps (9) are used to secure the hose. If it come loose from the tank then you need to tighten the clamps. If the filler neck is coming a loose from the fender area then you need to tighten or replace the fasteners there. It doesn''t look like it''s sold separately based on the two pictures below.\r\n\r\nIt''s item 7 in the pictures below.\r\n\r\nHere''s an image from another site.\r\n\r\nHere is a picture of one off a 2006. I see where I think you are talking about it coming apart but I don''t think you can get just that part.', 1420240224, 4, 9),
(13, 'BMW use valve overlap to serve EGR. Most if not all of BMW engines DO NOT have an EGR valve. They use a system on the engine that they have named Vaneos. This system changes the cam shaft timing so that it allows exhaust gas to remain in the cylinder during the inlet stroke. This has the same effect as an EGR system. www.bimmerforums.co.uk has a very good and detailed explanation.', 1420240341, 5, 9);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'Electrical'),
(2, 'Mechanical'),
(3, 'Technical'),
(4, 'Experience'),
(5, 'Maintenance');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `redirect_url` varchar(512) NOT NULL,
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `redirect_url`, `user_data`) VALUES
('a1ef198a277f71d50c63f88e7db55285', '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2251.0 Safari/537.36', 1420239235, '', 'a:1:{s:9:"user_data";s:0:"";}'),
('bdc195245db7b9cbf7760aa38c0a7e50', '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2251.0 Safari/537.36', 1420732192, '', 'a:1:{s:9:"user_data";s:0:"";}'),
('be2659a76c3ba0c4a5051db283565064', '::1', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2251.0 Safari/537.36', 1420238366, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `user_id` int(16) NOT NULL,
  `session_id` varchar(40) NOT NULL,
  KEY `id` (`user_id`),
  KEY `session_id` (`session_id`),
  KEY `user_id` (`user_id`),
  KEY `session_id_2` (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`user_id`, `session_id`) VALUES
(3, 'f9f2fa1ae74d75b7dd972ba6f169c6d4'),
(4, '57fa76b4bd3a80116e06122918441792'),
(1, 'a1ef198a277f71d50c63f88e7db55285'),
(1, 'bdc195245db7b9cbf7760aa38c0a7e50');

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE IF NOT EXISTS `question` (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `question` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `time` int(10) NOT NULL,
  `user_id` int(16) NOT NULL,
  `category_id` int(16) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`id`, `question`, `description`, `time`, `user_id`, `category_id`) VALUES
(1, 'Low beam headlights failing every 3-4 weeks', 'I have Volvo XC90 2006 and for the past few months every 3 - 4 weeks changing low beam light. Last time even did not last 5 days. Any recommendations why this light is failing?', 1420238695, 1, 1),
(2, 'Ford Fiesta and Focus Clutch Issues', 'I am in the UK and I am currently planning to get a Ford Focus/Fiesta depending on the budget and features. I have viewed 2 Fords so far - a Focus (100K miles, 2001 model) and a Fiesta (35k miles, 2008 model). The Focus had a very stiff clutch, but the Fiesta had a slightly smoother, but still relatively stiff clutch. The gear in the Fiesta is quite smooth, so I can feel that the car is releatively newer. Because I am used to a small Vauxhall Corsa which has a very good clutch.\r\n\r\nI have seen quite old cars such as Skoda with 66k miles, but the clutch was much better and smoother compared to Ford. That''s what confuses me the most.\r\n\r\nIs this issue quite common with the Fords? I am getting a feeling that the Fords are not very well with clutches. Does anyone has any experience. Also, If I can get the clutch work done and get it smoother, how much will it cost me?\r\n\r\nAny help/advice is appreciated!!!\r\n\r\nP.S. I also found the same problem with Renault Clios', 1420238862, 2, 2),
(3, 'Derbi Senda 50cc won''t run correctly?', 'I have a Derbi Senda SM 50 2005 plate. It has a good spark, however, it''s not reading any compression. It''s had a new top end and gaskets! What could be wrong?\r\n\r\nSometimes it starts when you bump start it, but then it will just die! Any suggestions?', 1420238979, 3, 2),
(4, 'What is the technical reason the engineer decided the motorcycle gear pattern as follows?', 'The gear pattern is selected by clicking a lever with your left foot, and is typically laid out as follows:\r\n\r\n6th gear (if applicable)\r\n5th gear\r\n4th gear\r\n3rd gear\r\n2nd gear\r\nNEUTRAL\r\n1st gear\r\nWhat is the technical reason the engineers decided the motorcycle gear pattern as above?\r\n\r\nMore precisely, why the NEUTRAL is placed between the first and second gears?', 1420239320, 4, 3),
(5, '2005 Ford Windstar constantly blows windshield wiper fuse', 'I''m curious what options there are for non-mechanics in diagnosing and correcting an issue where fuses for the Windshield Wipers on my 2005 Ford Windstar Mini-Van keep blowing? Upon placing the new fuse in the slot there is a snap and the fuse is blown.\r\n\r\nAny help is welcome!', 1420239466, 5, 2),
(6, '1993 Mazda MVP gets hot only when pressing on accelerator', 'We have replaced hoses and water pump and have had no problem with it getting hot. In the past week it has started to get hot, not overheated. It only happens after driving on highway when pressing the accelerator. Once you stop pressing on the gas pedal, the temperature drops. When you at a complete stop the temp drops down to the midway point on the gauge but when you go again the temp roses again.', 1420241575, 1, 2),
(7, 'Is it necessary to run the engine after adding radiator coolant or only when draining?', 'I recently did a timing belt/water pump change on my own. When I changed the water pump i drained the coolant, and according to my mechanic, I blew my radiator by not running the engine with the cap off after changing the coolant. According to the mechanic this lets air bubbles form in the radiator, which can cause pressure that damages the radiator.\r\n\r\nDo I need to run the engine with the radiator cap off whenever I add coolant to the radiator or only when I drain/flush the system?\r\n\r\nAlso, can I take off the cap to the coolant overflow container and fill that without needing to run the engine with the cap off the radiator?\r\n\r\nhonda civic 2001', 1420239837, 2, 4),
(8, 'What is the name of the part to which the gas cap connects?', 'I''m trying to find a replacement part for a 2005 Saturn ION.\r\n\r\nThere seems to be some detachable receptacle type thing to which the gas cap screws in. The gas cap itself is fine, but one of the plastic tabs that holds this receptacle in place has snapped off, and now the entire receptacle is loose and can come out of the gas tank. This exposes the entry point for gasoline into the gas tank, which I assume is fairly dangerous.\r\n\r\nWhen I search for parts, all that I can seem to find is the cap itself, which I do not need. I need the part to which the cap screws in.\r\n\r\nDoes anyone know what this part might be more appropriately named?\r\n\r\nPart that comes out of the gas tank.\r\n\r\nIn this picture, you can see the part that comes out of the gas tank filler hole. The gas cap is still screwed into it. Notice that it appears as though it is meant to lock into place on the car, but part of the locking mechanism has broken off.\r\n\r\nGas tank orifice with part removed.\r\n\r\nThis is the orifice where the part plugs in. I assume this is the top of the filler tube leading to the gas tank.', 1420239967, 3, 4),
(9, 'BMW E90 M47TU2 EGR clean', 'I have a BMW E90 2007 with a M47TU2 engine, Can anyone tell me where the EGR valve is, how to get it off and how to clean it (with what?)\r\n\r\nThanks', 1420240178, 4, 5),
(10, 'Oil change with front wheels on ramps?', 'From the obsessive compulsive department:\r\n\r\nI''ve been changing my own oil by driving up on a pair of ramps, draining and filling. Based on a slight overfill condition after adding the specified amount of oil and returning the vehicle to level, I know I''m not getting all the old oil out. This makes sense, since the drain plug is towards the front of the oil pan. Oil must be pooling at the back of the pan due to the angle from the ramps. It can''t be more than a couple or three ounces.\r\n\r\nI''m not worried about the slight excess. Shaving a couple ounces off the amount added next time will put it spot on. I don''t like the idea of leaving a measurable amount of dirty oil if I can avoid it, especially on the one vehicle that only gets out of the garage a few months a year.\r\n\r\nI suppose I can try and get another pair of ramps under the rear wheels if they''ll fit. Jacking up the rear to level the vehicle would work, too, I guess. Any other ideas? Am am I the only nut that worries about a couple ounces of dirty oil?', 1420240278, 5, 5),
(11, 'Why would my heater sometimes blow cold air?', 'This morning after running the car for 2 miles, my car was still blowing freezing cold air. I messed with the temp knob moving it all the way cold to all the way hot a few times. Then I started to get warm air. It happend again tonight. This time I had to turn off the car and turn it back on again. What could be going wrong? Where do I start?\r\n\r\nThis is on a ''98 Chevy Malibu.', 1420240393, 5, 3);

-- --------------------------------------------------------

--
-- Table structure for table `rate`
--

CREATE TABLE IF NOT EXISTS `rate` (
  `answer_id` int(16) NOT NULL,
  `user_id` int(16) NOT NULL,
  `rating` int(1) NOT NULL,
  KEY `answer_id` (`answer_id`,`user_id`),
  KEY `user_id` (`user_id`),
  KEY `answer_id_2` (`answer_id`),
  KEY `user_id_2` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rate`
--

INSERT INTO `rate` (`answer_id`, `user_id`, `rating`) VALUES
(8, 5, 4),
(10, 5, 5),
(6, 5, 3),
(7, 5, 4),
(9, 5, 2),
(7, 1, 1),
(6, 1, 5),
(9, 1, 4),
(4, 1, 4),
(5, 1, 1),
(13, 1, 3),
(6, 2, 2),
(1, 2, 2),
(3, 2, 1),
(5, 3, 4),
(3, 3, 2),
(12, 4, 2),
(1, 4, 4);

-- --------------------------------------------------------

--
-- Table structure for table `tagged_to`
--

CREATE TABLE IF NOT EXISTS `tagged_to` (
  `question_id` int(16) NOT NULL,
  `tag` varchar(32) NOT NULL,
  KEY `question_id` (`question_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tagged_to`
--

INSERT INTO `tagged_to` (`question_id`, `tag`) VALUES
(1, 'electrical'),
(1, 'volvo'),
(1, 'xc90'),
(2, 'clio'),
(2, 'clutch'),
(2, 'fiesta'),
(2, 'focus'),
(2, 'ford'),
(3, 'engine'),
(4, 'motorcycle'),
(5, 'ford'),
(5, 'windstar'),
(6, 'fuel-system'),
(6, 'fuel-tank'),
(6, 'mazda'),
(7, 'coolant'),
(7, 'radiator'),
(8, 'fuel-tank'),
(8, 'part-identification'),
(8, 'saturn'),
(9, 'bmw'),
(9, 'egr'),
(10, 'oil-change'),
(10, 'oil-pan'),
(11, 'chervrolet'),
(11, 'malibu');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(10) NOT NULL DEFAULT 'user',
  `name` varchar(128) DEFAULT NULL,
  `username` varchar(32) NOT NULL,
  `password` int(40) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `type`, `name`, `username`, `password`) VALUES
(1, 'admin', 'Yehan Pemarathne', 'yehancha', 7),
(2, 'user', 'Praminda Jayawardhana', 'pramindabj', 7),
(3, 'user', 'Anushka Samaranayake', 'anushka', 7),
(4, 'user', 'Grainier Perera', 'grainier', 7),
(5, 'user', 'Navin Pemarathne', 'navinyp', 7);

-- --------------------------------------------------------

--
-- Table structure for table `vote`
--

CREATE TABLE IF NOT EXISTS `vote` (
  `question_id` int(16) NOT NULL,
  `user_id` int(16) NOT NULL,
  KEY `question_id` (`question_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vote`
--

INSERT INTO `vote` (`question_id`, `user_id`) VALUES
(1, 2),
(1, 4),
(2, 2),
(2, 3),
(3, 1),
(4, 1),
(4, 2),
(4, 5),
(5, 5),
(6, 4),
(7, 4),
(7, 5),
(8, 2),
(8, 3),
(9, 1),
(9, 4),
(10, 1),
(10, 5);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `answer`
--
ALTER TABLE `answer`
  ADD CONSTRAINT `answer_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `answer_ibfk_2` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `login`
--
ALTER TABLE `login`
  ADD CONSTRAINT `login_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `question`
--
ALTER TABLE `question`
  ADD CONSTRAINT `question_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `question_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `rate`
--
ALTER TABLE `rate`
  ADD CONSTRAINT `rate_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `rate_ibfk_1` FOREIGN KEY (`answer_id`) REFERENCES `answer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tagged_to`
--
ALTER TABLE `tagged_to`
  ADD CONSTRAINT `tagged_to_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `vote`
--
ALTER TABLE `vote`
  ADD CONSTRAINT `vote_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `vote_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
